let game;

let gameOptions = {
	playerXOffset: 0,
    platformStartSpeed: 350,
    spawnRange: [100, 350],
    platformSizeRange: [50, 250],
    playerGravity: 900,
    jumpForce: 400,
    playerStartPosition: 100,
    jumps: 2
}

window.onload = function() {
    let gameConfig = {
        type: Phaser.AUTO,
        width: 1334,
        height: 750,
        scene: playGame,
        backgroundColor: 0x444444,

        physics: {
            default: "arcade"
        }
    }
    game = new Phaser.Game(gameConfig);
    window.focus();
    resize();
    window.addEventListener("resize", resize, false);
}

// playGame scene
class playGame extends Phaser.Scene{
    constructor(){
        super("PlayGame");
    }
    preload(){
        this.load.image("platform", "platform.png");
        this.load.image("player", "Mario.png");
    }
    create(){
        this.platformGroup = this.add.group({
            removeCallback: function(platform){
                platform.scene.platformPool.add(platform)
            }
        });

        this.platformPool = this.add.group({
            removeCallback: function(platform){
                platform.scene.platformGroup.add(platform)
            }
        });

        this.playerJumps = 0;

        this.addPlatform(game.config.width, game.config.width / 2);

        this.player = this.physics.add.sprite(gameOptions.playerStartPosition, game.config.height / 2, "player");
        this.player.setGravityY(gameOptions.playerGravity);

        this.physics.add.collider(this.player, this.platformGroup);
    }

    addPlatform(platformWidth, posX){
        let platform;
        if(this.platformPool.getLength()){
            platform = this.platformPool.getFirst();
            platform.x = posX;
            platform.active = true;
            platform.visible = true;
            this.platformPool.remove(platform);
        }
        else{
            platform = this.physics.add.sprite(posX, game.config.height * 0.8, "platform");
            platform.setImmovable(true);
            this.platformGroup.add(platform);
        }
        platform.displayWidth = platformWidth;
        this.nextPlatformDistance = Phaser.Math.Between(gameOptions.spawnRange[0], gameOptions.spawnRange[1]);
    }

    jump(){
        if(this.player.body.touching.down || (this.playerJumps > 0 && this.playerJumps < gameOptions.jumps)){
            if(this.player.body.touching.down){
                this.playerJumps = 0;
            }
            this.player.setVelocityY(gameOptions.jumpForce * -1);
            this.playerJumps ++;
        }
    }
	
	move_right(){
		if(gameOptions.playerXOffset < (game.config.width/2 - gameOptions.playerStartPosition)){
			gameOptions.playerXOffset += (1/30);
		}
		if(gameOptions.playerXOffset > (game.config.width/2 - gameOptions.playerStartPosition)){
			this.platformGroup.getChildren().forEach(function(platform){
				platform.setVelocityX(gameOptions.platformStartSpeed * -1);
			}, this);
		}
	}
	
	move_left(){
		if(gameOptions.playerXOffset > 0){
			gameOptions.playerXOffset -= (1/30);
		}
	}
	
	stop_moving(){
		this.platformGroup.getChildren().forEach(function(platform){
			platform.setVelocityX(0);
		}, this);
	}
	
	anyKeyMove(event){
		let code = event.keyCode;
		if(code === Phaser.Input.Keyboard.KeyCodes.D ||
		code === Phaser.Input.Keyboard.KeyCodes.RIGHT){
			this.move_right();
		}
		if(code === Phaser.Input.Keyboard.KeyCodes.A || 
		code === Phaser.Input.Keyboard.KeyCodes.LEFT){
			this.move_left();
		}
	}
	
	anyKeyStop(event){
		let code = event.keyCode;
		if(code != Phaser.Input.Keyboard.KeyCodes.SPACE ||
		code != Phaser.Input.Keyboard.KeyCodes.W ||
		code != Phaser.Input.Keyboard.KeyCodes.UP){
			this.stop_moving();
		}
	}
	
    update(){
        if(this.player.y > game.config.height){
            this.scene.start("PlayGame");
			gameOptions.playerXOffset = 0;
        }
        this.player.x = gameOptions.playerStartPosition + gameOptions.playerXOffset;

		this.input.keyboard.on('keydown_W', this.jump, this);
		this.input.keyboard.on('keydown_UP', this.jump, this);
		this.input.keyboard.on('keydown_SPACE', this.jump, this);
		
		this.input.keyboard.on('keydown', this.anyKeyMove, this);
		this.input.keyboard.on('keyup', this.anyKeyStop, this);

        let minDistance = game.config.width;
        this.platformGroup.getChildren().forEach(function(platform){
            let platformDistance = game.config.width - platform.x - platform.displayWidth / 2;
            minDistance = Math.min(minDistance, platformDistance);
            if(platform.x < - platform.displayWidth / 2){
                this.platformGroup.killAndHide(platform);
                this.platformGroup.remove(platform);
            }
        }, this);

        if(minDistance > this.nextPlatformDistance){
            var nextPlatformWidth = Phaser.Math.Between(gameOptions.platformSizeRange[0], gameOptions.platformSizeRange[1]);
            this.addPlatform(nextPlatformWidth, game.config.width + nextPlatformWidth / 2);
        }
    }
};

function resize(){
    let canvas = document.querySelector("canvas");
    let windowWidth = window.innerWidth;
    let windowHeight = window.innerHeight;
    let windowRatio = windowWidth / windowHeight;
    let gameRatio = game.config.width / game.config.height;
    if(windowRatio < gameRatio){
        canvas.style.width = windowWidth + "px";
        canvas.style.height = (windowWidth / gameRatio) + "px";
    }
    else{
        canvas.style.width = (windowHeight * gameRatio) + "px";
        canvas.style.height = windowHeight + "px";
    }
}
